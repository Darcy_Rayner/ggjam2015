﻿using UnityEngine;
using UnityEditor;
using System.IO;
using System.Collections;




namespace TMPro.EditorUtilities
{

    public static class TMPro_CreateSpriteAssetMenu
    {

        [MenuItem("Assets/Create/TextMeshPro - Sprite Asset", false, 100)]
        static void CreateTextMeshProObjectPerform(MenuCommand command)
        {
            Object target = Selection.activeObject;

            // Make sure the selection is a texture.
            if (target == null || target.GetType() != typeof(Texture2D))
                return;

            Texture2D sourceTex = Selection.activeObject as Texture2D;

            // Get the path to the selected texture.       
            string filePathWithName = AssetDatabase.GetAssetPath(sourceTex);
            string fileNameWithExtension = Path.GetFileName(filePathWithName);

            string filePath = filePathWithName.Replace(fileNameWithExtension, "");
                    
            // Create new Sprite Asset using this texture
            SpriteAsset spriteAsset = ScriptableObject.CreateInstance<SpriteAsset>();
            AssetDatabase.CreateAsset(spriteAsset, filePath + Path.GetFileNameWithoutExtension(fileNameWithExtension) + ".asset");

            // Setup TextureImporter to set source texture as Readable.        
            //TextureImporter texImporter = (TextureImporter)AssetImporter.GetAtPath(filePathWithName);
            //texImporter.isReadable = true; 
            //AssetDatabase.ImportAsset(texImporter.assetPath); 

            // Create a duplicate of the source texture.        
            //Texture2D spriteSheet = Object.Instantiate(sourceTex) as Texture2D;
            //spriteSheet.name = sourceTex.name;
            
            // Assign new Sprite Sheet texture to the Sprite Asset.
            spriteAsset.spriteSheet = sourceTex;
            spriteAsset.UpdateSpriteArray();
            //spriteSheet.hideFlags = HideFlags.HideInHierarchy;
            //AssetDatabase.AddObjectToAsset(spriteSheet, spriteAsset);

            // Get the Sprites contained in the Sprite Sheet
            
            


            //spriteAsset.sprites = sprites;

            // Set source texture back to Not Readable.
            //texImporter.isReadable = false;
          

            AssetDatabase.SaveAssets();

            //AssetDatabase.ImportAsset(AssetDatabase.GetAssetPath(spriteAsset));  // Re-import font asset to get the new updated version.
        }

       
    }
}