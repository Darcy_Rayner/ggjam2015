﻿using UnityEngine;
using System.IO;
using System.Collections;
using System.Collections.Generic;



namespace TMPro
{
    
    [System.Serializable]
    public class SpriteAsset : ScriptableObject
    {

        // The texture which contains the sprites.
        public Texture2D spriteSheet;

        // The material used to render these sprites.
        public Material material;

        // Array which contains all the sprites contained in the sprite sheet.
        public List<Sprite> sprites;



        void OnEnable()
        {
            if (sprites == null)
                LoadSprites();
        }


        public void AddSprites(string path)
        {

        }



#if UNITY_EDITOR
        public void UpdateSpriteArray()
        {
            string filePath = UnityEditor.AssetDatabase.GetAssetPath(spriteSheet);

            Object[] objects = UnityEditor.AssetDatabase.LoadAllAssetsAtPath(filePath);

            sprites = new List<Sprite>();

            foreach (Object obj in objects)
            {
                if (obj.GetType() == typeof(Sprite))
                {
                    Sprite sprite = obj as Sprite;
                    //Debug.Log("Sprite # " + sprites.Count + " Rect: " + sprite.rect);
                    sprites.Add(sprite);                              
                }
            }
        }
#endif


        void LoadSprites()
        {           
            //m_sprites = Resources.LoadAll<Sprite>(spriteSheet.name);

        }


    }
}
