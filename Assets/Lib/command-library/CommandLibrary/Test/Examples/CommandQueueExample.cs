using UnityEngine;
using System.Collections;
using CommandLibrary;

namespace CommandLibraryExample
{

public class CommandQueueExample : MonoBehaviour {

	// Use this for initialization
	void Start () 
	{
		_queue = new CommandQueue();
		BasicAnimation();
	}
	
	// Update is called once per frame
	void Update () 
	{
		_queue.Update(Time.deltaTime);
	}
		
	void BasicAnimation()
	{
		bool condition = false;
		_queue.Enqueue(
			Commands.ScaleTo(gameObject, 0.05f, 2.0f, Ease.OutQuart()),
			Commands.ScaleTo(gameObject, 1.0f, 1.0f, Ease.OutBounce()),
			Commands.RepeatForever(
				Commands.Parallel(
					Commands.Repeat(2,
						Commands.ScaleBy(gameObject, 1.5f, 1.0f, Ease.OutBounce())
					),
					Commands.RotateBy(gameObject, Quaternion.Euler(180.0f,0.0f, 90.0f), 0.25f, Ease.InOutHermite())
				),
				Commands.WaitForSeconds(0.25f),
				Commands.TintTo(gameObject, Color.red, 0.5f, Ease.InBack(0.2f)),
				Commands.TintBy(gameObject, Color.blue, 0.5f),
				Commands.Condition(delegate() { condition = !condition; return condition; }, 
					Commands.MoveBy(gameObject, new Vector3(0.0f, 2.0f, 0.0f), 0.25f, Ease.InOutHermite()),
					Commands.MoveBy(gameObject, new Vector3(0.0f, -2.0f, 0.0f), 0.25f, Ease.InOutHermite())
				),
				Commands.MoveTo(gameObject, new Vector3(0.0f, 0.0f, 0.0f), 0.25f, Ease.InOutHermite()),
				Commands.Parallel(
					Commands.ScaleTo(gameObject, 0.5f, 1.0f, Ease.OutBounce()),
					Commands.RotateTo(gameObject, Quaternion.identity, 0.5f, Ease.InOutHermite())
				),
				Commands.TintTo(gameObject, Color.white, 0.25f, Ease.InOutSin()),
				Commands.While(delegate(double elapsedTime) {
					return elapsedTime <= 0.5f; 	
				}),
				Commands.MoveFrom(gameObject,  new Vector3(0.0f, 0.0f, 0.8f), 0.5f, Ease.OutElastic()),
				Commands.RotateFrom(gameObject, Quaternion.Euler(0.0f, 45.0f, 45.0f), 0.5f, Ease.InOutExpo()),
				Commands.ScaleFrom(gameObject, 0.25f, 0.75f, Ease.InOutHermite()),
				Commands.TintFrom(gameObject, Color.green, 0.25f, Ease.InOutQuint())
			)
		);		
	}
	
	private CommandQueue _queue;
}

}
