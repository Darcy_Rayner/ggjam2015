using System;
using System.Collections;
using System.Collections.Generic;
using System.Reflection;
using CommandLibrary;
using System.Diagnostics;
using System.IO;

namespace CommandLibrary.Test
{

public partial class TestRunner
{
	private class TestFailedException : Exception 
	{
		public StackTrace DiagnosticStackTrace;
		public string DiagnosticeMessage;
		public TestFailedException(string message) : base(message) 
		{
			if (string.IsNullOrEmpty(message)) {
				message = "Failed Assert";
			}
			DiagnosticeMessage = message;
			DiagnosticStackTrace = new System.Diagnostics.StackTrace(2, true);
		}
	}

	#region Public properties

	public string CurrentTestName 
	{
		get { return _currentTestName == null ? "" : _currentTestName; }
	}

	public float Progress
	{
		get { return ((float)_progress) / _numTests; }
	}

	public Action<string, bool> OnLog { get; set; }
	public string SourcePath { get; set; }

	#endregion
	
	#region Static Methods

	public static TestRunner GenerateTests()
	{
		var runner = new TestRunner ();
		foreach (var type in GetTypesWithTestGroupAttribute(AppDomain.CurrentDomain.GetAssemblies())) {
			var classAttributes = type.GetCustomAttributes (typeof(TestGroupAttribute), true);
			var testGroupAttribute = (TestGroupAttribute) classAttributes[0];
			string groupName = testGroupAttribute.Name;
			if (groupName == null) {
				groupName = type.Name;				
			}
			foreach (MethodInfo method in type.GetMethods()) {
				bool isTest = false;
				string testName = null;
				foreach (var attribute in method.GetCustomAttributes(false)) {
					if (attribute is TestAttribute) {
						isTest = true;
						testName = ((TestAttribute) attribute).Name;
						break;
					}
				}

				if (isTest) {
					if (testName == null) { testName = method.Name; }
					testName = groupName + "::" + testName;
					var parameters = method.GetParameters();

					if (method.IsStatic && parameters.Length == 0) {
						MethodInfo localMethod = method;
						runner.Add (runner.Test(testName,localMethod));
					}
				}
			}
		}



		return runner;
	}
	
	#endregion

	#region Private static methods

	private static IEnumerable<Type> GetTypesWithTestGroupAttribute(Assembly [] assemblies) 
	{
		foreach (var assembly in assemblies) {
			foreach(Type type in assembly.GetTypes()) {
				if (type.GetCustomAttributes(typeof(TestGroupAttribute), true).Length > 0) {
					yield return type;
				}
			}
		}
	}

	#endregion

	#region Public Methods

	public void Add(params CommandDelegate[] tests)
	{
		foreach (var test in tests) {
			_queue.Enqueue(
				Commands.WaitForFrames(1),
				test,
				Commands.Do( () => ++_progress)
			);
		}
		_numTests += tests.Length;
	}

	public IEnumerator DoRun()
	{
		_queue.Enqueue(Commands.Do(() => {
			Log("---- " + _passedTests.Count + " Tests Passed ---- ");
			Log("---- " + _failedTests.Count + " Tests Failed ---- ");
		}));
		Log ("---- Running Unit Tests ----");
		
		_progress = 0;
		return _queue.WaitTillFinished(true);
	}

	#endregion

	#region Private methods

	private CommandDelegate Test(string name, MethodInfo test)
	{
		if (test == null) {
			throw new ArgumentNullException ("test");
		}

		return Commands.Do( () => {
			_currentTestName = name;
			var currentStackTrace = new StackTrace();
			int currentStackCount = currentStackTrace.GetFrames().Length + 3;
			try {
				test.Invoke(null, null);
			} catch (TargetInvocationException outerException) {
				if (outerException.InnerException is TestFailedException) {
					TestFailedException e = (TestFailedException) outerException.InnerException;
					_failedTests.Add(test.Name);
					var stackTrace = e.DiagnosticStackTrace;   // Skip the assert stack frame.
					var frames =  stackTrace.GetFrames();

					var builder = new System.Text.StringBuilder();
					builder
						.Append("Test Failed: ").Append(name)
						.Append("\nMessage: ").Append(e.DiagnosticeMessage)
						.Append("\nStack Trace: \n");
					// Only draw the relevant stack traces.
					for (int i = 0; i < frames.Length - currentStackCount; ++i) {
						var frame = frames[i];
						PrintStackFrame(builder, frame);
					}
					builder.Append("\n");
					Log(builder.ToString(), true);
					return;
				} else {
					_failedTests.Add(test.Name);

					Log("Test Failed: " + name + "\nException: " + outerException.InnerException.ToString(), true);
					return;
				}
			}

			_passedTests.Add(name);

			Log("Test Passed: " + name);
		});
	}

	private CommandDelegate Setup(CommandDo setup)
	{
		return Commands.Do( () => {
			try {
				setup();
			} catch (Exception e) {
				Log("Setup threw exception " + e.ToString(), true);
			}
		});
	}

	private void PrintStackFrame(System.Text.StringBuilder builder, StackFrame frame)
	{
		string filename =  frame.GetFileName ();
		if (SourcePath != null) {
			filename = filename.Substring (SourcePath.Length);
		}

		builder
			.Append (frame.GetMethod ().DeclaringType.FullName).Append (".").Append (frame.GetMethod ().Name);

		var parameters = frame.GetMethod ().GetParameters ();
		builder.Append ("(");
		for (int i = 0; i < parameters.Length; ++i) {
			var param = parameters[i];
			builder.Append(param.ParameterType.Name);
			if (i != parameters.Length - 1) {
				builder.Append(",");
			}
		}
		builder.Append (")");

		builder
			.Append(" (at ").Append(filename).Append(":").Append(frame.GetFileLineNumber()).Append(")")
			.Append("\n");
	}

	private void Log(string message, bool isError = false)
	{
		if (OnLog != null) { 
			OnLog (message, isError);
		}
	}


	#endregion

	#region Private fields
	private List<string> _failedTests = new List<string>();
	private List<string> _passedTests = new List<string>();
	private CommandQueue _queue = new CommandQueue();
	private string _currentTestName = null;
	private int _numTests = 0;
	private int _progress = 0;
	#endregion
}

}