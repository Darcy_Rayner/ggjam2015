using UnityEngine;
using System.Collections;
using CommandLibrary;

namespace CommandLibrary.Test
{

public class TestBehaviour : MonoBehaviour 
{
	// Use this for initialization
	void Start () 
	{
		TestRunner testRunner = TestRunner.GenerateTests ();
		StartCoroutine(testRunner.DoRun());
	}
}

}
