using System;
using System.Collections.Generic;
using UnityEngine;

namespace CommandLibrary.Test
{

public partial class TestRunner
{
	#region Static Methods

	public static void Assert(bool condition, string message = null)
	{
		if (!condition) {
			throw new TestFailedException(message);
		}
	}

	public static void AssertFalse(bool condition, string message = null)
	{
		if (condition) {
			throw new TestFailedException(message);
		}
	}

	public static void AssertEqual(object firstValue, object secondValue, string message = null)
	{
		if ((firstValue == null && secondValue == null) || 
			(firstValue != null && !firstValue.Equals(secondValue))) {
			string fullMessage = "Value " + firstValue + " != " + secondValue;
			fullMessage += message != null ? ", " + message : "";
			throw new TestFailedException (fullMessage);
		}
	}

	public static void AssertEqual<T>(T firstValue, T secondValue, IEqualityComparer<T> comparer, string message = null)
	{
		if (comparer.Equals(firstValue, secondValue)) {
			string fullMessage = "Value " + firstValue + " != " + secondValue;
			fullMessage += message != null ? ", " + message : "";
			throw new TestFailedException (fullMessage);
		}
	}

	public static void AssertNotEqual(object firstValue, object secondValue, string message = null)
	{
		if (firstValue == secondValue || 
			(firstValue != null && firstValue.Equals(secondValue)) ||
			(secondValue != null && secondValue.Equals(firstValue))) {
			string fullMessage = "Value " + firstValue + " == " + secondValue;
			fullMessage += message != null ? ", " + message : "";
			throw new TestFailedException (fullMessage);
		}
	}

	public static void AssertNotEqual<T>(T firstValue, T secondValue, IEqualityComparer<T> comparer, string message = null)
	{
		if (!comparer.Equals(firstValue, secondValue)) {
			string fullMessage = "Value " + firstValue + " == " + secondValue;
			fullMessage += message != null ? ", " + message : "";
			throw new TestFailedException (fullMessage);
		}
	}

	public static void AssertLessThan<T>(T firstValue, T secondValue, string message = null)
	{
		Comparer<T> comparer = Comparer<T>.Default;
		if (comparer.Compare(firstValue, secondValue) >= 0) {
			string fullMessage = "Value " + firstValue + " is not <" + secondValue;
			fullMessage += message != null ? ", " + message : "";
			throw new TestFailedException(fullMessage);
		}
	}

	public static void AssertLessThan<T>(T firstValue, T secondValue, IComparer<T> comparer, string message = null)
	{
		if (comparer.Compare(firstValue, secondValue) >= 0) {
			string fullMessage = "Value " + firstValue + " is not <" + secondValue;
			fullMessage += message != null ? ", " + message : "";
			throw new TestFailedException(fullMessage);
		}
	}

	public static void AssertLessThanOrEqual<T>(T firstValue, T secondValue, string message = null)
	{
		Comparer<T> comparer = Comparer<T>.Default;
		if (comparer.Compare(firstValue, secondValue) > 0) {
			string fullMessage = "Value " + firstValue + " is not <= " + secondValue;
			fullMessage += message != null ? ", " + message : "";
			throw new TestFailedException(fullMessage);
		}
	}

	public static void AssertLessThanOrEqual<T>(T firstValue, T secondValue, IComparer<T> comparer, string message = null)
	{
		if (comparer.Compare(firstValue, secondValue) > 0) {
			string fullMessage = "Value " + firstValue + " is not <=" + secondValue;
			fullMessage += message != null ? ", " + message : "";
			throw new TestFailedException(fullMessage);
		}
	}

	public static void AssertApprox(double firstValue, double secondValue, double tolerance, string message = null)
	{
		if ( Math.Abs(firstValue - secondValue) > tolerance) {
			string fullMessage = "Value " + firstValue + " isn't approximately " + secondValue + " with tolereance " + tolerance;
			fullMessage += message != null ? ", " + message : "";
			throw new TestFailedException(fullMessage);
		}
	}

	#endregion
}

}


