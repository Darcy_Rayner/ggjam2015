using System;
using UnityEngine;

namespace CommandLibrary.Test
{

/// <summary>
/// Unity specific assertions for TestRunner.
/// </summary>
public partial class TestRunner
{
	#region Static Methods
	public static void AssertApprox(Vector4 firstValue, Vector4 secondValue, float tolerance, string message = null)
	{
		if ( Math.Abs(firstValue.x - secondValue.x) > tolerance ||
			Math.Abs(firstValue.y - secondValue.y) > tolerance ||
			Math.Abs(firstValue.z - secondValue.z) > tolerance ||
			Math.Abs(firstValue.w - secondValue.w) > tolerance) {
			string fullMessage = "Value " + firstValue + " isn't approximately " + secondValue + " with tolereance " + tolerance;
			fullMessage += message != null ? ", " + message : "";
			throw new TestFailedException(fullMessage);
		}
	}

	public static void AssertApprox(Quaternion firstValue, Quaternion secondValue, float tolerance, string message = null)
	{
		if ( Math.Abs(1.0f - Quaternion.Dot(firstValue, secondValue)) > tolerance) {
			string fullMessage = "Value " + firstValue + " isn't approximately " + secondValue + " with tolereance " + tolerance;
			fullMessage += message != null ? ", " + message : "";
			throw new TestFailedException(fullMessage);
		}
	}

	public static void AssertApprox(Rect firstValue, Rect secondValue, float tolerance, string message = null)
	{
		if ( Math.Abs(firstValue.x - secondValue.x) > tolerance ||
			Math.Abs(firstValue.y - secondValue.y) > tolerance ||
			Math.Abs(firstValue.width - secondValue.width) > tolerance ||
			Math.Abs(firstValue.height - secondValue.height) > tolerance) {
			string fullMessage = "Value " + firstValue + " isn't approximately " + secondValue + " with tolereance " + tolerance;
			fullMessage += message != null ? ", " + message : "";
			throw new TestFailedException(fullMessage);
		}
	}
	#endregion
}

}

