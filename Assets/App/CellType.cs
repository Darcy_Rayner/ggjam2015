﻿using UnityEngine;
using System;

namespace Game.Model
{

public class CellType 
{
	public const uint MAX_COLOUR_INDEX = 6;
	public const uint MAX_SYMBOL_INDEX = 20;
	
	public enum SubType
	{
		Normal,
		LeftEye,
		RightEye,
		Forehead,
		LeftCheek,
		RightCheek,
		Chin,
		Nose
	}

	public readonly uint ColourIndex;
	public readonly uint SymbolIndex; 
	public readonly SubType Type;

	public CellType(uint colourIndex, uint symbolIndex, SubType subType)
	{
		if (colourIndex >= MAX_COLOUR_INDEX || symbolIndex > MAX_SYMBOL_INDEX){
			throw new InvalidOperationException();
		}
		ColourIndex = colourIndex;
		SymbolIndex = symbolIndex;
		Type = subType;
	}
}

}
