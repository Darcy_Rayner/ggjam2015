﻿using UnityEngine;
using System.Collections;
using CommandLibrary;
using Game.Model;
using TMPro;
using Game.Utils;
using TouchScript.Gestures;
using System;

namespace Game.View
{

public class HexCell : MonoBehaviour 
{
	#region Constants
	
	private Color[] Colours;
	private Color[] GlowColours;
	private const float RAISE_OFFSET = 30.0f;
	private const float SHAKE_OFFSET = 90.0f;
	private const float DOWN_OFFSET = -15.0f;
	private const float FULL_DROP_OFFSET = -800.0f;
	
	private enum State
	{
		Blank,
		Raised,
		Dropped
	}
	
	#endregion
	
	#region MonoBehaviour fields
	
	public SpriteRenderer PillarSprite;
	public SpriteRenderer ReversePillarSprite;
	public ParticleSystem Particles;
	public SpriteRenderer GlowSprite;
	public SpriteRenderer ColourSprite;
	public SpriteRenderer BevelSprite;
	public TextMeshPro CountText; 
	
	public Sprite[] Sprites;
	public Sprite Forehead;
	public Sprite LeftEye;
	public Sprite RightEye;
	public Sprite Nose;
	public Sprite Chin;
	public Sprite LeftCheek;
	public Sprite RightCheek;
	
	
	[Serializable]
	public class SymbolPairs
	{
		public Sprite Bevel;
		public Sprite Glow;
		public Sprite Color;
	}
	
	public SymbolPairs[] Pairs;
	
	#endregion
	
	#region Public properties


	public CellType CellType 
	{
		get { return _cellType; }
		set 
		{
			_cellType = value;

		}
	} 
	
	public int SortingOrder
	{
		get { return _sortingOrder; }
		set 
		{ 
			_sortingOrder = value;
			PillarSprite.sortingOrder = value;
			ReversePillarSprite.sortingOrder = value;
			ColourSprite.sortingOrder = value;
			GlowSprite.sortingOrder = value;
			BevelSprite.sortingOrder = value;
			CountText.sortingOrder = value;
			Particles.renderer.sortingOrder = value;
			
		}
	}
	
	public int Count
	{
		get { return _count; }
		set 
		{
			_count = value;
		}
	}
	
	public event Action<HexCell> OnCellPressed;
	
	#endregion
	
	#region Public methods
	
	public void Raise()
	{
		Ref<Color> colorRef = new Ref<Color>( 
			() => GlowSprite.color,
			(t) => GlowSprite.color = t
		);	
		if (_state != State.Raised) {
			_queue.Enqueue(
				Commands.Parallel(
					Commands.MoveTo(PillarSprite.gameObject, new Vector3(0.0f, InitialY + RAISE_OFFSET), 0.3f, Ease.OutBack(), true),
					Commands.AlphaTo(colorRef, 1.0f, 0.3f, Ease.Smooth())
				),
				Commands.Do(StartGlow)
			);	
			_state = State.Raised;
			EnableTouch();
		}
	}
	
	public void ShakeRaise()
	{
		Ref<Color> colorRef = new Ref<Color>( 
			() => GlowSprite.color,
			(t) => GlowSprite.color = t
		);
		
		if (_state != State.Raised) {
			_queue.Enqueue(
				Commands.Do(UpdateCountText),	
				Commands.MoveTo(PillarSprite.gameObject, new Vector3(0.0f, InitialY + SHAKE_OFFSET), 0.9f, Ease.InOutBack(), true),
				Commands.Parallel(
					Commands.MoveTo(PillarSprite.gameObject, new Vector3(0.0f, InitialY + RAISE_OFFSET), 0.9f, Ease.OutBack(), true),
					Commands.AlphaTo(colorRef, 1.0f, 0.3f, Ease.Smooth())
				),
				Commands.Do(StartGlow)
			);
			_state = State.Raised;
			EnableTouch();		
		}
	}
	
	public void Lower()
	{
		Ref<Color> colorRef = new Ref<Color>( 
			() => GlowSprite.color,
			(t) => GlowSprite.color = t
		);
		if (_state != State.Blank) {
			_queue.Enqueue(
				Commands.Do(StopGlow),	
				Commands.Parallel(
					Commands.MoveTo(PillarSprite.gameObject, new Vector3(0.0f, InitialY + DOWN_OFFSET), 0.3f, Ease.Linear(), true),
					Commands.AlphaTo(colorRef, 0.0f, 0.3f, Ease.Smooth())
				)
			);
			_state = State.Blank;
			DisableTouch();
		}
	}
	
	public void Shake()
	{
		_queue.Enqueue(
			Commands.Do(UpdateCountText),	
			Commands.MoveTo(PillarSprite.gameObject, new Vector3(0.0f, InitialY + SHAKE_OFFSET), 0.9f, Ease.InOutBack(), true),
			Commands.MoveTo(PillarSprite.gameObject, new Vector3(0.0f, InitialY), 0.9f, Ease.InOutBack(), true)
		);	
		_state = State.Blank;
		DisableTouch();
	}
	
	public void FullDrop()
	{
		_queue.Enqueue(
			Commands.Do( () => Particles.Play()),
			Commands.Parallel(
				gameObject.Rumble(10.0f, 1.2), 				
				Commands.MoveTo(PillarSprite.gameObject, new Vector3(0.0f, InitialY + FULL_DROP_OFFSET), 0.9f, Ease.InQuad(), true)
			),
			Commands.Do(UpdateCountText),
			Commands.Do( () => Particles.Stop())
		);
		
		if (_state == State.Raised) {
			DisableTouch();		
		}
		_state = State.Dropped;
	}
	
	public void FullBlank()
	{
		if (_state == State.Dropped) {
			_queue.Enqueue(
				Commands.MoveTo(PillarSprite.gameObject, new Vector3(0.0f, InitialY), 1.9f, Ease.OutQuad(), true)
			);
		}
		_state = State.Blank;	
	}
	
	public void FullRaise()
	{
		Ref<Color> colorRef = new Ref<Color>( 
			() => GlowSprite.color,
			(t) => GlowSprite.color = t
		);
		
		if (_state != State.Raised) {
			_queue.Enqueue(
				Commands.Parallel(
					Commands.MoveTo(PillarSprite.gameObject, new Vector3(0.0f, InitialY + RAISE_OFFSET), 1.9f, Ease.OutQuad(), true),
					Commands.AlphaTo(colorRef, 1.0f, 0.3f, Ease.Smooth())
				)
			);
			EnableTouch();
		}
		_state = State.Raised;	
	}
	
	#endregion
	
	#region MonoBehaviour methods
	
	private void Start()
	{
		Colours = new Color[] {
			new Color(142.0f / 255.0f, 167.0f / 255.0f, 94.0f / 255.0f),
			new Color(138.0f / 255.0f, 101.0f / 255.0f, 77.0f / 255.0f),
			new Color(108.0f / 255.0f, 162.0f / 255.0f, 206.0f / 255.0f),
			new Color(85.0f / 255.0f, 144.0f / 255.0f, 127.0f / 255.0f),
			new Color(157.0f / 255.0f, 199.0f / 255.0f, 176.0f / 255.0f),	
			new Color(114.0f / 255.0f, 116.0f / 255.0f, 152.0f / 255.0f)
		};
		
		GlowColours = new Color[] {
			new Color(203.0f / 255.0f, 220.0f / 255.0f, 170.0f / 255.0f),
			new Color(206.0f / 255.0f, 190.0f / 255.0f, 167.0f / 255.0f),
			new Color(136.0f / 255.0f, 184.0f / 255.0f, 225.0f / 255.0f),
			new Color(155.0f / 255.0f, 220.0f / 255.0f, 201.0f / 255.0f),
			new Color(216.0f / 255.0f, 190.0f / 255.0f, 208.0f / 255.0f),
			new Color(185.0f / 255.0f, 206.0f / 255.0f, 235.0f/ 255.0f)
		};
		var colour = GlowSprite.color;
		colour.a = 0.0f;
		GlowSprite.color = colour;
		InitialY = PillarSprite.transform.localPosition.y + UnityEngine.Random.Range(-5f, 5.0f);
		PillarSprite.transform.localPosition = new Vector3(0.0f, InitialY);
		int index = UnityEngine.Random.Range(0, Sprites.Length);
		
		PillarSprite.sprite = Sprites[index];
		ReversePillarSprite.sprite = Sprites[index];
		
		ConfigureCell(_cellType);
	}
	
	private void Update()
	{
		_queue.Update(Time.deltaTime);
		_glowQueue.Update(Time.deltaTime);
		_textQueue.Update(Time.deltaTime);
	}
	
	
	#endregion
	
	#region Private methods
	
	private void UpdateCountText()
	{
		if (_cellType.Type == CellType.SubType.Forehead) {
		
			Ref<Color> alphaRef = new Ref<Color>(
				() => CountText.color,
				(t) => CountText.color = t
			);
			
			_textQueue.Enqueue(
				Commands.AlphaTo(alphaRef, 0.0f, 0.3f, Ease.Smooth()),
				Commands.Do(() => CountText.text = string.Format("{0}", _count)),
				Commands.AlphaTo(alphaRef, 1.0f, 0.3f, Ease.Smooth())
			);
		}
	}		
	
	private void ConfigureCell(CellType type)
	{
		var color = Colours[type.ColourIndex];
		var glowColor = GlowColours[type.ColourIndex];
		glowColor.a = GlowSprite.color.a;

		var pair = Pairs[type.SymbolIndex];
		ColourSprite.sprite = pair.Color;
		ColourSprite.color = color;
		BevelSprite.sprite = pair.Bevel;
		GlowSprite.sprite = pair.Glow;
		GlowSprite.color = glowColor;
		
		ColourSprite.enabled = (type.Type == CellType.SubType.Normal);
		BevelSprite.enabled = (type.Type == CellType.SubType.Normal);
		GlowSprite.enabled = (type.Type == CellType.SubType.Normal);
		CountText.gameObject.SetActive(type.Type == CellType.SubType.Forehead);
		var colour = CountText.color;
		colour.a = 0.0f;
		CountText.color = colour;
		
		switch (_cellType.Type) {
			case CellType.SubType.Forehead:
				PillarSprite.sprite = Forehead;
				break;
			case CellType.SubType.Chin:
				PillarSprite.sprite = Chin;
				break;
			case CellType.SubType.Nose:
				PillarSprite.sprite = Nose;
				break;			
			case CellType.SubType.LeftEye:
				PillarSprite.sprite = LeftEye;
				break;
			case CellType.SubType.RightEye:
				PillarSprite.sprite = RightEye;
				break;
			case CellType.SubType.LeftCheek:
				PillarSprite.sprite = LeftCheek;
				break;
			case CellType.SubType.RightCheek:
				PillarSprite.sprite = RightCheek;
				break;
			}
	}
	
	private void HandlePressed (object sender, System.EventArgs e)
	{
		if (OnCellPressed != null) {
			OnCellPressed(this);
		}
	}
	
	private void EnableTouch()
	{
		var pressGesture = PillarSprite.gameObject.ObtainComponent<PressGesture>();
		pressGesture.enabled = true;
		pressGesture.Pressed += HandlePressed;
	}
	
	private void DisableTouch()
	{
		var pressGesture = PillarSprite.gameObject.ObtainComponent<PressGesture>();
		pressGesture.enabled = false;
		pressGesture.Pressed -= HandlePressed;
	}
	
	private void StartGlow()
	{
		Ref<float> alphaRef = new Ref<float>(
			() => GlowSprite.color.a,
			(t) => {
				Color color = GlowSprite.color;
				color.a = t;
				GlowSprite.color = color;
			}
		);
	
		_glowQueue.Enqueue(
			Commands.RepeatForever(
				Commands.PulsateScale(alphaRef, -0.6f, 2.0f)
			)
		);
	}
	
	private void StopGlow()
	{
		_glowQueue = new CommandQueue();
	}
	
	#endregion
	
	#region Private fields
	
	private float InitialY;
	private int _sortingOrder;
	private State _state = State.Blank;
	private CellType _cellType = new CellType(0, 0, CellType.SubType.Normal);
	private CommandQueue _queue = new CommandQueue();
	private CommandQueue _glowQueue = new CommandQueue();
	private CommandQueue _textQueue = new CommandQueue();
	private int _count = 0;
	
	#endregion
}

}
