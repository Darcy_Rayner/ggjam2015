﻿using UnityEngine;
using System.Collections.Generic;
using Game.Model;
using Game.Utils;
using CommandLibrary;

namespace Game.View
{

public class GameRoot : MonoBehaviour 
{
	public const float CAMERA_Y_OFFSET = 80.0f;

	public HexGrid Grid;
	public AudioClip ClickSound;
	public AudioClip CollapseSound;
	public List<AudioClip> ShiftSounds;
	public SpriteRenderer Title;
	public SpriteRenderer Gradient;
	public SpriteRenderer Lines;
	public SpriteRenderer Play;

	// Use this for initialization
	void Start () 
	{
		Application.targetFrameRate = 60;
		List<CellType> cellTypes = new List<CellType>();
		for (uint colourIndex = 0; colourIndex < CellType.MAX_COLOUR_INDEX; ++colourIndex) {
			for (uint symbolIndex = 0; symbolIndex < CellType.MAX_SYMBOL_INDEX; ++symbolIndex) {
				var cellType = new CellType(colourIndex, symbolIndex, CellType.SubType.Normal);
				cellTypes.Add(cellType);
			}
		}
		
		Grid.Populate();
		
		var queue = new Queue<CellType>(cellTypes.Shuffle());
		int rowIndex = 0;
		foreach (var row in Grid.Rows) {
			int colIndex = 0;
			foreach (var cell in row.Cells) {
				cell.CellType = null;
				if (colIndex == 4) {
					if (rowIndex == 2) {
						cell.CellType = new CellType(0, 0, CellType.SubType.LeftEye);
					} else if (rowIndex == 3) {
						cell.CellType = new CellType(0, 0, CellType.SubType.LeftCheek);		
					}
				} else if (colIndex == 5) {
					if (rowIndex == 2) {
						cell.CellType = new CellType(0, 0, CellType.SubType.Forehead);
						_countCell = cell;
						_countCell.Count = 0;
					} else if (rowIndex == 3) {
						cell.CellType = new CellType(0, 0, CellType.SubType.Nose);
					} else if (rowIndex == 4) {
						cell.CellType = new CellType(0, 0, CellType.SubType.Chin);	
					}
				} else if (colIndex == 6) {
					if (rowIndex == 2) {
						cell.CellType = new CellType(0, 0, CellType.SubType.RightEye);
					} else if (rowIndex == 3) {
						cell.CellType = new CellType(0, 0, CellType.SubType.RightCheek);		
					}
				}
				if (cell.CellType == null) {
					cell.CellType =  queue.Dequeue();	
				}

				cell.OnCellPressed += HandleOnCellPressed;
				
				_cells.Add(cell);
				++colIndex;
			} 
			++rowIndex;
		}
		
		_audioManager = gameObject.ObtainComponent<AudioManager>();
		
		
		var properties = new AudioManager.AudioClipProperties(AudioManager.AudioClipProperties.Mode.PitchRandom, 0.2f, 0f, 2, 0.25f);
		foreach (var clip in ShiftSounds) {
			_audioManager.SetClipProperties(clip, properties);
		}
		
		_audioManager.SetClipProperties(ClickSound, new AudioManager.AudioClipProperties(AudioManager.AudioClipProperties.Mode.Ascending, 0.1f, 2.0f));	
		
		_unusedCells = new List<HexCell>(_cells);
		
		_originalOrthoSize = Camera.main.orthographicSize;
		Camera.main.orthographicSize = Camera.main.orthographicSize * 0.7f;
		var position = Camera.main.transform.position;
		position.y += CAMERA_Y_OFFSET;
		Camera.main.transform.position = position;
		
		Play.transform.parent = _countCell.PillarSprite.transform;
		RaiseFirst();
	}

	private void HandleOnCellPressed(HexCell cell)
	{
		if (cell == _sequence[_sequenceNumber]) {
			cell.Lower();
			_sequenceNumber += 1;
			_audioManager.PlayClip(ClickSound);	
			if (!_firstButtonPressed) {
				_firstButtonPressed = true;
				IntroSequence();
			} else if (_sequence.Count == _sequenceNumber) { 
				RaiseNext(cell.transform.localPosition); 
			}
		} else {
			Reset(cell.transform.localPosition);
		}
	}
	
	private void RaiseFirst()
	{
		HexCell nextCell = _countCell;
		int index = _unusedCells.IndexOf(nextCell);
		_unusedCells.RemoveAt(index);
		_sequence.Add(nextCell);
		_sequenceNumber = 0;
		_countCell.Count = _score;
		_score += 1;
		
		Ref<float> alphaRef = new Ref<float>(
			() => Gradient.color.a,
			(t) => {
				Color color = Gradient.color;
				color.a = t;
				Gradient.color = color;
		});
			
		_gradientQueue.Enqueue(
			Commands.RepeatForever(
				Commands.PulsateScale(alphaRef, -0.2f, 5.0f)
			)
		);
		
		_queue.Enqueue(
			Commands.WaitForSeconds(1.0f),
			Commands.Do( () => {
				_audioManager.PlayClip(ShiftSounds.GetRandom());
				_countCell.Raise();	
			})
		);
	}
	
	private void RaiseNext(Vector3 fromPosition)
	{
		int index = UnityEngine.Random.Range(0, _unusedCells.Count);
		HexCell nextCell = _unusedCells[index];
		if (_sequence.Count == 0) {
			nextCell = _countCell;
			index = _unusedCells.IndexOf(nextCell);
		}
		_unusedCells.RemoveAt(index);
		_sequence.Add(nextCell);
		_sequenceNumber = 0;
		_countCell.Count = _score;	
		_score += 1;
			
		_queue.Enqueue(
			Commands.WaitForSeconds(0.3),
			Commands.Parallel(
				_unusedCells.ForEachCommand( (c) => {
					float rand = UnityEngine.Random.Range(0.001f, 0.15f);
					float distance = (c.transform.localPosition - fromPosition).magnitude;
					return Commands.Sequence(
						Commands.WaitForSeconds(rand + distance / 1024.0f),
						Commands.Do(() => _audioManager.PlayClip(ShiftSounds.GetRandom())),
						Commands.Do(c.Shake),
						Commands.WaitForSeconds(0.5f),
						Commands.Do(() => _audioManager.PlayClip(ShiftSounds.GetRandom()))
					);
				}),
				
				_sequence.ForEachCommand ( (c) => {
					float rand = UnityEngine.Random.Range(0.001f, 0.15f);
					float distance = (c.transform.localPosition - fromPosition).magnitude;
				
					return Commands.Sequence(
						Commands.WaitForSeconds(rand + distance / 1024.0f),
						Commands.Do(() => _audioManager.PlayClip(ShiftSounds.GetRandom())),
						Commands.Do(c.ShakeRaise),
						Commands.WaitForSeconds(0.5f),
						Commands.Do(() => _audioManager.PlayClip(ShiftSounds.GetRandom()))
					);
				})
			) 
		);
	}
	
	
	private void Reset(Vector3 fromPosition)
	{
		foreach (var cell in _sequence) {
			cell.Lower();
		}
		
		_unusedCells = new List<HexCell>(_cells);
			
		_sequenceNumber = 0;
		_score = 0;
		_sequence.Clear();
		
		HexCell nextCell = _countCell;
		_unusedCells.Remove(nextCell);
		_sequence.Add(nextCell);
		
		_queue.Enqueue(
			Commands.Do( () => _audioManager.PlayClip(CollapseSound)),
			Commands.WaitForSeconds(0.3),
			Commands.Parallel(
				gameObject.Rumble(10.0f, 1.2), 
				_cells.ForEachCommand( (c) => {
					float rand = UnityEngine.Random.Range(0.001f, 0.15f);
					float distance = (c.transform.localPosition - fromPosition).magnitude;
					return Commands.Sequence(
						Commands.WaitForSeconds(rand + distance / 1024.0f),
						Commands.Do(c.FullDrop)
					);
				})
			),
			Commands.Do(() => _countCell.Count = 0),
			Commands.WaitForSeconds(0.9f),
			Commands.Parallel(
				_unusedCells.ForEachCommand(c => Commands.Do(c.FullBlank)),
				Commands.Do(nextCell.FullRaise)
			)
		);
	}
	
	private void Update()
	{
		_queue.Update(Time.deltaTime);
		_gradientQueue.Update(Time.deltaTime);
	}
	
	private void IntroSequence()
	{
		_countCell.Lower();
		
		Ref<float> orthoRef = new Ref<float>(
			() => Camera.main.orthographicSize,
			(t) => Camera.main.orthographicSize = t		
		);
		
		_gradientQueue = new CommandQueue();
		
		_queue.Enqueue(
			Commands.AlphaTo(Play, 0.0f, 0.1, Ease.Smooth()),	
			Commands.Parallel(
				gameObject.Rumble(10.0f, 0.5f),
				Commands.AlphaTo(Title, 0.0f, 0.3, Ease.Smooth()),
				Commands.AlphaTo(Gradient, 0.0f, 0.3, Ease.Smooth()),
				Commands.AlphaTo(Lines, 0.0f, 0.3, Ease.Smooth()),
				Commands.MoveBy(Camera.main.gameObject, Vector3.down * CAMERA_Y_OFFSET, 0.3, Ease.OutQuad()),
				Commands.ChangeTo(orthoRef, _originalOrthoSize, 0.3, Ease.Smooth())
			),
			Commands.Do(() => RaiseNext(_countCell.transform.position))
		);
	}
	
	private float _originalOrthoSize = 0.0f;
	private bool _firstButtonPressed = false;
	private HexCell _countCell;
	private int _score = 0;
	private CommandQueue _queue = new CommandQueue();
	private CommandQueue _gradientQueue = new CommandQueue();
	private int _sequenceNumber = 0;
	private List<HexCell> _sequence = new List<HexCell>();
	private List<HexCell> _unusedCells = new List<HexCell>();
		
	private List<HexCell> _cells = new List<HexCell>();
	private List<HexCell> _centreCells = new List<HexCell>();
	private AudioManager _audioManager;
	
}

}