﻿using UnityEngine;
using System.Collections.Generic;
using CommandLibrary;
using Game.Utils;

namespace Game.View
{

public class HexGrid : MonoBehaviour
{
	#region MonoBehaviour fields

	public int NumberOfRows;
	public int NumberOfColumns;
	public float RowOffset;
	public float ColumnOffset;
	public float ColumnStagger;
	
	public HexCell HexCellPrefab;
	
	#endregion
	
	#region Public properties
	
	public IEnumerable<Row> Rows
	{
		get { return _rows.AsReadOnly(); }
	}
	
	#endregion

	#region Public methods
	
	public void Populate()
	{
		for (int i = 0; i < NumberOfRows; ++i) {
			Row row = new Row();
			
			row.Cells = new List<HexCell>();
			for (int j = 0; j < NumberOfColumns; ++j) {
				if (i == 0 && j % 2 == 1) { continue; }
				float x = (j - (NumberOfColumns - 1) / 2.0f) * ColumnOffset;	
				float y = -(i - (NumberOfRows - 1) / 2.0f) * RowOffset + (j % 2) * ColumnStagger;	
				
				var cell = Instantiate(HexCellPrefab) as HexCell;
				cell.SortingOrder = i * 2 +((j + 1) % 2);
				
				cell.transform.SetParent(transform, false);
				cell.transform.localPosition = new Vector3(x, y, 0.0f);
				row.Cells.Add(cell);
			}
			_rows.Add(row);
		}

		/*gameObject.Queue(
			Commands.WaitForSeconds(1.0),
			gameObject.ForEachCommand<HexCell>( (cell) => {
				return Commands.Sequence(
					Commands.WaitForSeconds(1.00f + cell.transform.localPosition.magnitude / 768.0f),
					Commands.Do(cell.Shake)
				);
			})
		);*/
		
	}
	
	#endregion

	#region Private types

	public struct Row
	{
		public List<HexCell> Cells;
	}
	
	private List<Row> _rows = new List<Row>();
	
	#endregion
}

}
