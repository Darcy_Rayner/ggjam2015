﻿using UnityEngine;
using System.Collections.Generic;
using CommandLibrary;

namespace Game.View
{

public class AudioManager : MonoBehaviour 
{
	#region Constants
	
	private const float SEMI_TONES_COEFFICIENT = 1.05946f;	
	
	#endregion
	
	#region Public types
	
	public struct AudioClipProperties
	{
		public enum Mode
		{
			OneShot,
			Ascending,
			PitchRandom
		}
		
		public AudioClipProperties(Mode mode, float minimumSeperation = 0f, float ascendingTimeout = 1f, int maxSimultaneous = 5, float volume = 1f)
		{
			CurrentMode = mode;
			MinimumSeperation = minimumSeperation;
			AscendingTimeout = ascendingTimeout;
			MaxSimultaneous = maxSimultaneous;
			Volume = volume;
		}
		
		public readonly Mode CurrentMode;
		public readonly float MinimumSeperation;
		public readonly float AscendingTimeout ;
		public readonly int MaxSimultaneous;
		public readonly float Volume;
	}
	
	#endregion
	
	#region Public methods
	
	public void PlayClip(AudioClip clip)
	{
		if (!_clipProperties.ContainsKey(clip)) {
			_clipProperties.Add(clip, new AudioClipProperties(AudioClipProperties.Mode.OneShot));			
		}
		if (!_playingClips.ContainsKey(clip)) {
			_playingClips.Add(clip, new PlayInfo());
		}
			
		AudioClipProperties properties = _clipProperties[clip];
		PlayInfo info = _playingClips[clip];
		
		float timeDelta = Time.time - _playingClips[clip].LastPlayedTimeStamp;
		if (properties.MinimumSeperation > timeDelta || properties.MaxSimultaneous <= info.AudioSources.Count) {
			return;
		}
		
		var source = gameObject.AddComponent<AudioSource>();
		source.volume = properties.Volume;
		
		switch (properties.CurrentMode) {
			case AudioClipProperties.Mode.OneShot:

				break;
			case AudioClipProperties.Mode.Ascending:
			{
				if (timeDelta > properties.AscendingTimeout) {
					info.AscendingCount = 0;
				} else {
					info.AscendingCount++;
				}
				float pitch = Mathf.Pow(SEMI_TONES_COEFFICIENT, info.AscendingCount);
				pitch = Mathf.Clamp(pitch, 1.0f, 32.0f);
				source.pitch = pitch;
				break;
			}
			case AudioClipProperties.Mode.PitchRandom:
			{
				int shiftAmount = UnityEngine.Random.Range(-2, 2);
	
				float pitch = Mathf.Pow(SEMI_TONES_COEFFICIENT, shiftAmount);
				pitch = Mathf.Clamp(pitch, 1.0f, 32.0f);
				source.pitch = pitch;
				break;
			}
		}
		
		_scheduler.Add(Play(clip, source));
	}
	
	public void SetClipProperties(AudioClip clip, AudioClipProperties properties)
	{
		_clipProperties[clip] = properties;
	}
	
	
	#endregion
	
	#region MonoBehaviour events
	
	private void Update()
	{
		_scheduler.Update(Time.deltaTime);
	}
	
	#endregion
	
	#region Private methods
	
	private CommandDelegate Play(AudioClip clip, AudioSource source)
	{
		
		source.clip = clip;
		source.Play();
		_playingClips[clip].AudioSources.Add(source);
		_playingClips[clip].LastPlayedTimeStamp = Time.time;
		
		return Commands.Sequence(
			Commands.While( (t) => source.isPlaying),
			Commands.Do( () => {
				_playingClips[clip].AudioSources.Remove(source);
				GameObject.Destroy(source);
			})
		);
		
	}
	
	#endregion
	
	#region Private types
	
	private class PlayInfo
	{
		public bool IsPlaying { get { return AudioSources.Count > 0; } }
		public List<AudioSource> AudioSources = new List<AudioSource>();
		public float LastPlayedTimeStamp = float.NegativeInfinity;
		public int AscendingCount = 0;
	}
	
	#endregion
	
	#region Private fields
	
	private CommandScheduler _scheduler = new CommandScheduler();
	private Dictionary<AudioClip, AudioClipProperties> _clipProperties = new Dictionary<AudioClip, AudioClipProperties>();
	private Dictionary<AudioClip, PlayInfo> _playingClips = new Dictionary<AudioClip, PlayInfo>();
	
	#endregion

}

}
