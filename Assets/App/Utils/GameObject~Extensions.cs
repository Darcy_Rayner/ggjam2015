﻿using UnityEngine;
using System.Collections.Generic;
using CommandLibrary;
using System;

namespace Game.Utils
{

public static class GameObjectExtensions 
 {
	public static T ObtainComponent<T>(this GameObject gm) where T : Component
	{
		var component = gm.GetComponent<T>();
		if (!component) {
			component = gm.AddComponent<T>();
		}
		return component;
	}
	
	public static CommandDelegate ForEachCommand<T>(this GameObject gm, Func<T, CommandDelegate> func) where T : Component
	{
		var commands = new List<CommandDelegate>();
		foreach (T comp in gm.GetComponentsInChildren<T>()) {
			commands.Add(func(comp));
		}
		return Commands.Parallel(commands.ToArray());
	}
}

}
