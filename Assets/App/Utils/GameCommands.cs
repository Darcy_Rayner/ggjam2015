﻿using UnityEngine;
using CommandLibrary;

namespace Game.Utils
{

public static class GameCommands 
{
	public static CommandDelegate Rumble(this GameObject gm, float amount, double duration)
	{
		Vector3 basePosition = Vector3.zero;
		float sqrt2 = Mathf.Sqrt(2f);
		return Commands.Sequence(
			Commands.Do( () => basePosition = gm.transform.localPosition),
			Commands.Duration( (t) => {
				var offset = new Vector3(
					UnityEngine.Random.Range(-amount / sqrt2, amount / sqrt2),
					UnityEngine.Random.Range(-amount / sqrt2, amount / sqrt2)
				);
				gm.transform.localPosition = basePosition + offset;
			}, duration),
			Commands.Do(() => gm.transform.localPosition = basePosition)
		);
	}
}

}
