﻿using System.Collections.Generic;
using CommandLibrary;
using System;

namespace Game.Utils
{

public static class IEnumerableExtensions
{
	public static CommandDelegate ForEachCommand<T>(this IEnumerable<T> enumerable, Func<T, CommandDelegate> func)
	{
		var commands = new List<CommandDelegate>();
		foreach (T comp in enumerable) {
			commands.Add(func(comp));
		}
		return Commands.Parallel(commands.ToArray());
	}
}

}
