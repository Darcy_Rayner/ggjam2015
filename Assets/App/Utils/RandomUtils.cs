﻿using UnityEngine;
using System.Collections.Generic;

namespace Game.Utils
{

public static class RandomUtils 
{
	public static List<T> Shuffle<T>(this IEnumerable<T> values)
	{
		List<T> list = new List<T>(values);
		for (int i = 0; i < list.Count; ++i) {
			int index = UnityEngine.Random.Range(0, list.Count);
			var firstValue = list[index];
			list[index] = list[i];
			list[i] = firstValue; 
		}
		return list;
	}	
	
	public static T GetRandom<T>(this List<T> values)
	{
		int index = UnityEngine.Random.Range(0, values.Count);
		return values[index];	
	}
	
}

}
